from MiniMaxPlayer import MiniMaxPlayer
from MiniMaxAlphaBetaPlayer import MiniMaxAlphaBetaPlayer
from game import Game, Player, RandomPlayer
from time import time

if __name__ == '__main__':
    SIZE = 8
    game = Game(SIZE)
    
    maximizer = MiniMaxAlphaBetaPlayer(SIZE)
    minimizer = MiniMaxAlphaBetaPlayer(SIZE)
    
    maximizer.initialize('B')
    minimizer.initialize('W')
    
    tic = time()
    game.playOneGame(maximizer, minimizer, True)
    toc = time()
    print(toc - tic)
    print(minimizer.numOfMoves)
 
