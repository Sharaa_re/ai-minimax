from game import Game, Player
from time import time

class MiniMaxPlayer(Game, Player):
	def initialize(self, side):
		self.side = side
		self.name = "MiniMax Agent"
		self.maxScore = 1
		self.maxDepth = 5
		self.numOfMoves = 0

	def getMove(self, board):
		tic = time()
		self.numOfMoves += 1     
		moves = self.generateMoves(board, self.side)
		move = ([], 0)
		if len(moves) != 0:
			move = self.miniMax(self.side, board, 1)
		toc = time()
		print('move value', move[1], 'dicision time', toc - tic)
		return move[0]

	def movingBeads(self, moves):
		beads = set()
		for move in moves:
			beads.add((move[0], move[1]))

		return len(beads)

	def evaluation(self, player, board, depth):
		moves = self.generateMoves(board, player)
		bestVal = 0
		if player == 'B':
			bestVal = 0
			for move in moves:
				nextBoard = self.nextBoard(board, player, move)
				opponentMoves = self.generateMoves(nextBoard, 'W')
				if len(opponentMoves) == 0:
					return (None, self.maxScore)
				values = list(map(lambda opMove: len(self.generateMoves(self.nextBoard(nextBoard, 'W', opMove), 'B')), opponentMoves))
				opChosen = min(values)
				if opChosen > bestVal:
					bestVal = opChosen
			return (None, bestVal)
		else:
			bestVal = self.maxScore
			for move in moves:
				nextBoard = self.nextBoard(board, player, move)
				opponentMoves = len(self.generateMoves(nextBoard, 'B'))
				if opponentMoves < bestVal:
					bestVal = opponentMoves
		
			return (None, bestVal)

	def evalWeightedMoveDiff(self, player, board, depth):
		moves = self.generateMoves(board, player)
		moveValues = sum(list(map(lambda move: self.moveValue(player, move), moves)))
		opperantMoves = self.generateMoves(board, 'W' if player == 'B' else 'B')
		oppMoveValues = sum(list(map(lambda move: self.moveValue(player, move), opperantMoves)))
		beadsNumber = self.countSymbol(board, 'B') + self.countSymbol(board, 'W')
		diff = moveValues - oppMoveValues
		return (None, diff / beadsNumber) if player == 'B' else (None, -1 * diff/ beadsNumber)

	def evalMoveDiff(self, player, board, depth):
		moves = self.generateMoves(board, player)
		opperantMoves = self.generateMoves(board, 'W' if player == 'B' else 'B')
		diff = len(moves) - len(opperantMoves)
		beadsNumber = self.countSymbol(board, 'B') + self.countSymbol(board, 'W')
		emptyNumber = self.countSymbol(board, '.')
		x = self.size * self.size  - abs(beadsNumber - emptyNumber)
		return (None, diff / x) if player == 'B' else (None, -1 * diff / x)
	

	def moveValue(self,player, move):
		moveValue = int
		dis = self.distance(move[0], move[1], move[2], move[3])
		if dis == 2:
			moveValue = 1
		else:
			moveValue = 2

		if move[3] == 0 or move[2] == 0 or move[3] ==self.size - 1 or move[2] == self.size -1:
			moveValue =  moveValue / 2
		if move[0] == 0 or move[1] == 0 or move[0] ==self.size - 1 or move[1] == self.size -1:
			moveValue =  moveValue * 2
			
		return moveValue
			

	def utility(self, player, depth):
		return (None, self.maxScore) if player == 'W' else (None, -1 * self.maxScore)

	def miniMax(self, player, board, depth):
		moves = self.generateMoves(board, player)
		if len(moves) == 0:
			return self.utility(player, depth)

		if depth >= self.maxDepth:
			return self.evalMoveDiff(player, board, depth)

		if player == 'B':
			bestVal = -self.maxScore - 1
			bestMove = []
			moveValues = []
			for move in moves:
				nextBoard = self.nextBoard(board, player, move)
				value = self.miniMax('W',nextBoard, depth + 1)[1]
				moveValues.append((move, value))
				if value > bestVal:
					bestVal = value
					bestMove = move
			return (bestMove, bestVal)
		if player == 'W':
			bestVal = self.maxScore + 1
			bestMove = tuple()
			moveValues = []
			for move in moves:
				nextBoard = self.nextBoard(board, player, move)
				value = self.miniMax('B',nextBoard, depth + 1)[1]
				moveValues.append((move, value))
				if value < bestVal:
					bestVal = value
					bestMove = move			
			return (bestMove, bestVal)

