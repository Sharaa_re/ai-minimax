from MiniMaxPlayer import MiniMaxPlayer
from time import time

class MiniMaxAlphaBetaPlayer(MiniMaxPlayer):
    def initialize(self, side):
        self.side = side
        self.name = 'MiniMax Alpha Beta Player'
        self.maxDepth = 5
        # self.maxScore = int(self.size * self.size / 2)
        self.maxScore = 1
        self.numOfMoves = 0
        
    def getMove(self, board):
        tic = time()
        self.numOfMoves += 1
        # input()
        moves = self.generateMoves(board, self.side)
        # print('possible moves', moves)
        move = ([], 0)
        if len(moves) != 0:
            move = self.miniMax(self.side, board, 1, -self.maxScore - 1, self.maxScore + 1)
        toc = time()
        print('move value', move[1], 'dicision time', toc - tic)        
        return move[0]
            
    def miniMax(self, player, board, depth, alpha, beta):
        # print('player', player,'depth', depth, 'alpha', alpha, 'beta', beta)
        moves = self.generateMoves(board, player)
        if len(moves) == 0:
            return self.utility(player, depth)
        
        if depth >= self.maxDepth:
            return self.evalMoveDiff(player, board, depth)
        
        elif player == 'B':
            bestVal =  -self.maxScore - 1
            bestMove = []
            for move in moves:
                nextBoard = self.nextBoard(board, player, move)
                value = self.miniMax('W', nextBoard, depth + 1, alpha, beta)[1]
                if value > bestVal:
                    bestVal = value
                    bestMove = move
                alpha = max(bestVal, alpha)
                if beta <= alpha:
                    return (bestMove, bestVal)
                    
            return (bestMove, bestVal)
        
        else:
            bestVal = self.maxScore + 1
            bestMove = []
            for move in moves:
                nextBoard = self.nextBoard(board, player, move)
                value = self.miniMax('B', nextBoard, depth + 1, alpha, beta)[1]
                if value < bestVal:
                    bestVal = value
                    bestMove = move
                beta = min(bestVal, beta)
                if beta <= alpha:
                    return (bestMove, bestVal)
                    
            return (bestMove, bestVal)
            
        